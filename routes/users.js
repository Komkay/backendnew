const express = require('express')
const router = express.Router()
const usersController = require('../controller/UsersController')

router.get('/', usersController.getUsers)

router.get('/:id', usersController.getUsers)

router.post('/', usersController.addUser)

router.put('/', usersController.updateUser)

router.delete('/:id', usersController.deleteUser)

module.exports = router
